<?php
if($_POST['cr_facultatives'] && $_SESSION['form_submit_cr_facultatives'] == false)
{
    if($_POST['discipline_name']=='' || strlen($_POST['discipline_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали Название дисциплины!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['new_modified']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Статус!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['semester_year']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Длительность!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['year_of_approbation']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Год апробации!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['level']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Уровень!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['number_of_students']=='')
    {
        $postError =  "<br><span style='color: red;'>Вы не заполнили Количество студентов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['language']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Язык!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    elseif($_POST['specialty_code']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Код направления!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_facultatives.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_facultatives.php");
}
?>
