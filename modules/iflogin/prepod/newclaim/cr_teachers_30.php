<?php
if($_POST['cr_teachers_30'] && $_SESSION['form_submit_cr_teachers_30'] == false)
{
    if($_POST['teacher_age']=='')
    {
        $postError =  "<br><span style='color: red;'>Вы не заполнили возраст!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_teachers_30.php");
    }
    elseif($_POST['teacher_degree']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали степень!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_teachers_30.php");
    }
    elseif($_POST['teacher_loading']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали нагрузку!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_teachers_30.php");
    }
    elseif($_POST['teacher_rate_share']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не указали долю ставки!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_teachers_30.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_teachers_30.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_teachers_30.php");
}
?>
