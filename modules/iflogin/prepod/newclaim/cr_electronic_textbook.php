<?php
if($_POST['cr_electronic_textbook'] && $_SESSION['form_submit_cr_electronic_textbook'] == false)
{
    if($_POST['textbook_authors']=='' || strlen($_POST['textbook_authors'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали авторов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    if($_POST['textbook_name']=='' || strlen($_POST['textbook_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали название!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    if($_POST['basic_course']=='' || strlen($_POST['basic_course'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали базовый курс!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    if($_POST['web_url']=='' || strlen($_POST['web_url'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали электронный адрес!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    elseif($_POST['year_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали год издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    elseif($_POST['month_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали месяц издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    elseif($_POST['number_of_sheets']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали количество печатных листов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    elseif($_POST['language']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Язык!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_electronic_textbook.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_electronic_textbook.php");
}
?>
