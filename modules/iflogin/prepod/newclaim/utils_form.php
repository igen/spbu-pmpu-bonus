<?php

function label_id( $fieldid )
{
    $fieldid .= '_label';
    return $fieldid;
}


function table_begin( $border )
{
    return "<table border='". $border . "' width='100%' align='center'>
        <tr>
            <td>\n";
}

function table_end()
{
    return "
            </td>
        </tr>
    </table>\n";
}


function form_begin( $caption, $border, $error )
{
    return "<form method='post' onsubmit='return checkForm(this);'>
                <table cellpadding='5px' border='$border' align='center'>
                    <tr>
                         <td colspan='2' align='center'>$caption $error</td>
                    </tr>
    ";
}

function form_end( $dbtable )
{
    return "
                    <tr>
                        <td align='center' colspan='2'>
                        <input type='submit' name='".$dbtable."' value='Отправить заявку'>
                        </td>
                    </tr>
                    </form>
                </table>\n";
}


function form_row__text_input($label, $fieldid, $maxlength, $size )
{
    return "
                    <tr>
                        <td align='right' id='" . label_id($fieldid) . "'>$label</td>
                        <td align='left'><input type='text' name='$fieldid' value='".$_POST[$fieldid]."' maxlength='$maxlength' size='$size'></td>
                    </tr>
    ";
}

function form_row__combobox($label, $fieldid, $options )
{
    $row = "
                    <tr>
                        <td align='right' id='". label_id($fieldid) ."' >$label</td>
                        <td align='left'>
                            <select name='$fieldid' size='1' >
								<option selected='selected' value='0'>-</option>\n";
    for ($i = 1; $i < count($options); $i++) {
        $row .= "<option value='$i'>".$options[$i]."</option>\n";
    }
    $row . "					 </select>
                        </td>
                    </tr>";
    return $row;
}

?>
