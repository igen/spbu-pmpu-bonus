<?php
if($_POST['cr_tutorials'] && $_SESSION['form_submit_cr_tutorials'] == false)
{
    if($_POST['tutorial_authors']=='' || strlen($_POST['tutorial_authors'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали авторов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    if($_POST['tutorial_name']=='' || strlen($_POST['tutorial_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали название!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    if($_POST['tutorial_type']=='' || strlen($_POST['tutorial_type'])>50)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали тип!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    if($_POST['electronic_printed']=='' || strlen($_POST['electronic_printed'])>20)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали _electronic_printed_!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    if($_POST['web_url']=='' || strlen($_POST['web_url'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали электронный адрес!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    elseif($_POST['year_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали год издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    elseif($_POST['month_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали месяц издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    elseif($_POST['number_of_sheets']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали количество печатных листов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    elseif($_POST['publishing_house']=='' || strlen($_POST['publishing_house'])>255 )
    {
        $postError =  "<br><span style='color: red;'>Вы не заполнили издательство!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    elseif($_POST['language']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Язык!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_tutorials.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_tutorials.php");
}
?>
