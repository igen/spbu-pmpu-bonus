<?php

include("$doc_root/modules/iflogin/prepod/newclaim/utils_form.php");

$_SESSION['form_submit_cr_teachers_30'] = false;
echo table_begin($config_table_border_value)
    . form_begin('Заявка на премию', $config_table_border_value, $postError)
    . form_row__text_input('Возраст', 'teacher_age', 11, 4)
    . form_row__combobox('Степень', 'teacher_degree', $config_array_teacher_degree)
    . form_row__text_input('Общая нагрузка', 'teacher_loading', 11, 4)
    . form_row__combobox('Доля ставки', 'teacher_rate_share', $config_array_teacher_rate)
    . form_end('cr_teachers_30')
    . table_end();
?>
