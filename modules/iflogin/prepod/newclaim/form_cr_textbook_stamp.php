<?php


include("$doc_root/modules/iflogin/prepod/newclaim/utils_form.php");

$_SESSION['form_submit_cr_textbook_stamp'] = false;
echo table_begin($config_table_border_value)
    . form_begin('Заявка на премию', $config_table_border_value, $postError)
    . form_row__text_input('Авторы', 'textbook_authors', 255, 50)
    . form_row__text_input('Название', 'textbook_name', 255, 50)
    . form_row__combobox('Год издания', 'year_of_publication', $config_array_year_of_publication)
    . form_row__combobox('Месяц издания', 'month_of_publication', $config_array_month)
    . form_row__text_input('Количество печатных листов', 'number_of_sheets', 11, 4)
    . form_row__text_input('Издательство', 'publishing_house', 255, 50)
    . form_row__combobox('Язык', 'language', $config_array_language)
    . form_end('cr_textbook_stamp')
    . table_end();
?>
