<?php


include("$doc_root/modules/iflogin/prepod/newclaim/utils_form.php");

$_SESSION['form_submit_cr_facultatives'] = false;
echo table_begin($config_table_border_value)
    . form_begin('Заявка на премию', $config_table_border_value, $postError)
    . form_row__text_input('Название дисциплины', 'discipline_name', 255, 50)
    . form_row__combobox('Статус', 'new_modified', $config_array_new_modified)
    . form_row__combobox('Длительность', 'semester_year', $config_array_semester_year)
    . form_row__combobox('Год апробации', 'year_of_approbation', $config_array_year_of_approbation)
    . form_row__combobox('Уровень', 'level', $config_array_level)
    . form_row__text_input('Количество студентов', 'number_of_students', 11, 4)
    . form_row__combobox('Код направления', 'specialty_code', $config_array_specialty_code)
    . form_row__combobox('Язык', 'language', $config_array_language)
    . form_end('cr_facultatives')
    . table_end();
?>
