<?php
if($_POST['cr_lab_works'] && $_SESSION['form_submit_cr_lab_works'] == false)
{
    if($_POST['lab_work_name']=='' || strlen($_POST['lab_work_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали Название лабораторной работы!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['lab_work_number']=='')
    {
        $postError =  "<br><span style='color: red;'>Вы не задали Количество лабораторных работ!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['discipline_name']=='' || strlen($_POST['discipline_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали Название дисциплины!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['new_modified']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Статус!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['specialty_code']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Код направления!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['oop_code']=='' || strlen($_POST['oop_code'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали Шифр ООП!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    elseif($_POST['language']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Язык!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_lab_works.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_lab_works.php");
}
?>
