<?php
if($_POST['cr_textbook_stamp'] && $_SESSION['form_submit_cr_textbook_stamp'] == false)
{
    if($_POST['textbook_authors']=='' || strlen($_POST['textbook_authors'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали авторов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    if($_POST['textbook_name']=='' || strlen($_POST['textbook_name'])>255)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали название!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    elseif($_POST['year_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали год издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    elseif($_POST['month_of_publication']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали месяц издания!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    elseif($_POST['number_of_sheets']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не задали количество печатных листов!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    elseif($_POST['publishing_house']=='' || strlen($_POST['publishing_house'])>255 )
    {
        $postError =  "<br><span style='color: red;'>Вы не заполнили издательство!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    elseif($_POST['language']==0)
    {
        $postError =  "<br><span style='color: red;'>Вы не выбрали Язык!</span>";
        include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
    }
    else
    {
        include("$doc_root/modules/iflogin/prepod/newclaim/exec_cr_textbook_stamp.php");
    }
}
else
{
    include("$doc_root/modules/iflogin/prepod/newclaim/form_cr_textbook_stamp.php");
}
?>
