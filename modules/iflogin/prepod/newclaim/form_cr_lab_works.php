<?php


include("$doc_root/modules/iflogin/prepod/newclaim/utils_form.php");

$_SESSION['form_submit_cr_lab_works'] = false;
echo table_begin($config_table_border_value)
    . form_begin('Заявка на премию', $config_table_border_value, $postError)
    . form_row__text_input('Название лабораторной работы', 'lab_work_name', 255, 50)
    . form_row__text_input('Количество лабораторных работ', 'lab_work_number', 11, 4)
    . form_row__text_input('Название дисциплины', 'discipline_name', 255, 50)
    . form_row__combobox('Код направления', 'specialty_code', $config_array_specialty_code)
    . form_row__text_input('Шифр ООП', 'oop_code', 255, 50)
    . form_row__combobox('Статус', 'new_modified', $config_array_new_modified)
    . form_row__combobox('Язык', 'language', $config_array_language)
    . form_end('cr_lab_works')
    . table_end();
?>
