<?php

if(isset($_GET['action']) && isset($_GET['criteria']))
{
    include("$doc_root/modules/iflogin/prepod/newclaim/newclaim_criteria_form.php");
}
else if(isset($_GET['action']))
{
    switch($_GET['action']){
        case 'newclaim':
            include("$doc_root/modules/iflogin/prepod/newclaim/newclaimform.php");
            break;
        case 'claimshelp':
            include("$doc_root/modules/iflogin/prepod/claimshelp/claimshelpform.php");
            break;
        case 'news':
            include("$doc_root/modules/iflogin/prepod/news/news.php");
            break;
        }
}
else
{
    include("$doc_root/modules/iflogin/prepod/my_claims.php");
}

?>
