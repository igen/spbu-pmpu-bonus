<?php

$getAllCriteria = mysql_query("SELECT * FROM $config_table_criteria");

echo "
<table border='$config_table_border_value' width='100%'>

    <tr>
        <td align='center'><h3>Выберите критерий по которому требуется списать баллы:</h3></td>
    </tr>
    <tr>
        <td>
            <table align=center width='600px' border='0'>
            <tr>
                <td align=left><b>Критерий</b></td>
                <td align=right><b>Доступные баллы</b></td>
                <td align=right><b>Выбрать</b></td>
            </tr>
            ";

        while( $row=mysql_fetch_assoc($getAllCriteria) )
        {
			$criteriaTable = $row['criteria_name'];
			$pointsForPay = 0;
			
			// для каждого критерия требуется вывести баланс баллов:
			// 	- из таблицы <criteria_name> выбираем все утвержденные балы - это положительная составляющая балланса 
			//	- из таблицы debit отбираем по критерию кол-во всех баллов - это отрищательная часть балланса
			// 	- разницу помещаем в поле - это кол-во баллов, которое еще не оприходовано деньгами
		
			$getSignedPoints = mysql_query("SELECT SUM(points) FROM $criteriaTable WHERE dep_head_visa=1");
			if( $rowSignedPoints = mysql_fetch_array($getSignedPoints) )
			{
				$pointsForPay = $rowSignedPoints[0];
			}
			mysql_free_result($getSignedPoints);

			$getPaidPoints = mysql_query("SELECT SUM(points) FROM $config_table_debit WHERE criteria_id=$row[criteria_id]");
			if( $rowPaidPoints = mysql_fetch_array($getPaidPoints) )
			{
				$pointsForPay = $pointsForPay - $rowPaidPoints[0];
			}
			mysql_free_result($getPaidPoints);
					
			$action = $pointsForPay > 0 ? "<a href='$root/?action=debit&criteria=$row[criteria_name]'>Списать...</a>" : "Нет баллов";
            echo "<tr>
                    <td align=left>
                        $row[criteria_num] $row[criteria_name_rus]
                    </td>
                    <td align=right>
                        $pointsForPay
                    </td>
                    <td align=right>
                        $action
                    </td>
                  </tr>";
        }

		mysql_free_result($getAllCriteria);
echo"</table>
    </td>
    </tr>
    </table>";
?>
