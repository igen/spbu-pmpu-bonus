<?php

$_SESSION['form_submit_debit'] = false;

$criteriaName   = mysql_real_escape_string($_GET['criteria']);
$queryCriteria  = mysql_query("SELECT * FROM $config_table_criteria WHERE `criteria_name`='$criteriaName'");
$rowCriteria    = mysql_fetch_assoc($queryCriteria);
$criteriaId		= $rowCriteria['criteria_id'];

if( mysql_num_rows($queryCriteria) == 1 )
{
    $queryTeachers = mysql_query(
        "SELECT  prep.teacher_id AS prep_id
                , prep.surname AS prep_surname
                , prep.name AS prep_name
                , prep.secondname AS prep_secondname
                , prep.department AS prep_department
                , cr.dep_head_date AS cr_date
                , SUM(cr.points) AS cr_points
				, (SELECT SUM(points) FROM $config_table_debit WHERE teacher_id=prep.teacher_id AND criteria_id=$criteriaId) AS dt_points
            FROM $criteriaName AS cr
                , active_teachers AS prep
            WHERE cr.teacher_id=prep.teacher_id
              AND cr.dep_head_visa=1
            GROUP BY prep_id
			"
        );

    $error = "";
    if( !$queryTeachers ) {
		print_error("Ошибка доступа к базе данных", mysql_error());
        exit;
    }

	$teacherIDs 	= array();
	$teacherNames	= array();
	$teacherDeps	= array();
	$teacherPoints	= array();
	$teacherKeys	= array();	// инфа (id+points) будет передаваться через POST
	
	$i = 0;
	while( $rowTeacher = mysql_fetch_assoc($queryTeachers) )
	{
		$signPoints = (int)$rowTeacher['cr_points'];
		$paidPoints = (int)$rowTeacher['dt_points'];
		$workPoints = $signPoints - $paidPoints;
		
		if( $workPoints > 0 )
		{
			$teacherIDs[$i] 	= $rowTeacher['prep_id'];
			$teacherNames[$i]	= $rowTeacher['prep_surname']." ".$rowTeacher['prep_name']." ".$rowTeacher['prep_secondname'];
			$teacherDeps[$i]	= $rowTeacher['prep_department'];
			$teacherPoints[$i]	= $workPoints;
			$teacherKeys[$i]	= $rowTeacher['prep_id'] . ':' . $workPoints;
			$i++;
		}
	}
	
    if( count($teacherIDs) > 0 )
	{
        echo "
            <h1>Списание баллов по критерию: $rowCriteria[criteria_name_rus] </h1> "
            . table_begin($config_table_border_value)
            . form_begin($config_table_border_value, $error)
            . "<table align=center width='100%' border='0'>
                <tr>
                    <td align=right></td>
                    <td align=left><b>Преподаватель</b></td>
                    <td align=left><b>Кафедра</b></td>
                    <td align=right><b>Баллы</b></td>
                </tr>
                ";

        for( $j = 0; $j < count($teacherIDs); $j++)
        {
            echo "<tr>
                    <td align=right>
                        <input type='checkbox' name='teachers[]' value='".$teacherKeys[$j]."' checked />
                    </td>
                    <td align=left>
                        " . $teacherNames[$j] . "
                    </td>
                    <td align=left>
                        " . $teacherDeps[$j] . "
                    </td>
                    <td align=right>
                        " . $teacherPoints[$j] . "
                    </td>
                </tr>";
        }
        echo"</table> ";
        echo form_money_input('Распределить сумму: ', 'money')
            . form_end()
            . table_end();
    }
    else
    {
        echo "<h3>Нет баллов</h3>";
    }

}

?>
