<?php

include("$doc_root/modules/iflogin/boss/boss_utils.php");

if( $_POST['exec_debit'] && $_SESSION['form_submit_debit'] == false )
{
	$totalSum = (float)mysql_real_escape_string($_POST['money']);
	
    if( $totalSum <= 0 )
    {
		print_error("Укажите общую сумму списания баллов!", "");
        include("$doc_root/modules/iflogin/boss/debit/debit_form.php");
    }
	elseif( count($_POST['teachers']) == 0 )
	{
		print_error("Не выбран ни один преподаватель", "");
        include("$doc_root/modules/iflogin/boss/debit/debit_form.php");	
	}
	else
	{		
		// OK!
		include("$doc_root/modules/iflogin/boss/debit/debit_exec.php");
	}	
}
else
{
	include("$doc_root/modules/iflogin/boss/debit/debit_form.php");
}
?>
