<?php

$criteriaName   = mysql_real_escape_string($_GET['criteria']);
$queryCriteria  = mysql_query("SELECT * FROM $config_table_criteria WHERE `criteria_name`='$criteriaName'");
$rowCriteria    = mysql_fetch_assoc($queryCriteria);
$criteriaId		= $rowCriteria['criteria_id'];

$totalSum_rub	= (float)mysql_real_escape_string($_POST['money']);	// сумма в реблях
$teacherIDs 	= array();
$teacherPoints 	= array();
$totalPoints 	= 0;

for( $i = 0; $i < count($_POST['teachers']); $i++)
{
	list($id, $points)	= split(":", $_POST['teachers'][$i]);
	$teacherIDs[$i] 	= (int)$id;
	$teacherPoints[$i] 	= (int)$points;
	$totalPoints 		+= (int)$points;
}

$pointSum_kop = 100 * (float)$totalSum_rub / $totalPoints; // стоимость одного балла (в копейках)

// начисление денег по каждому преподавателю
if( !mysql_query("START TRANSACTION") )
{
	print_error("Возникла ошибка при выполнении внутренней операции", mysql_error());
	exit();
}


$isOk = false;
$errDetail = "";

for( $i = 0; $i < count($_POST['teachers']); $i++)
{
	$teacherSum = (float)$teacherPoints[$i] * $pointSum_kop;
	$teacherId	= $teacherIDs[$i];
	$points 	= $teacherPoints[$i];
	
	$isOk = mysql_query("INSERT INTO $config_table_debit 
							( admin_id
							, teacher_id
							, criteria_id
							, date
							, points
							, amount_of_money ) 
						VALUES 	('$system_activeUserId'
								, '$teacherId'
								, '$criteriaId'
								, now()
								, '$points'
								, '$teacherSum' )
						");
				
	if( !$isOk ){
		$errDetail = mysql_error();
		break;
	}
}

if( $isOk )
{
	mysql_query("COMMIT");
	echo "<h2>Процедура начисления денег по критерию $rowCriteria[criteria_name_rus] выполнена успешно</h2>";
}
else 
{
	mysql_query("ROLLBACK");
	print_error("Ошибка при обращении к базе данных", $errDetail);
}


?>