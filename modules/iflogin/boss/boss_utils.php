<?php

function label_id( $fieldid )
{
    $fieldid .= '_label';
    return $fieldid;
}


function table_begin( $border )
{
    return "<table border='". $border . "' width='100%' align='center'>
        <tr>
            <td>\n";
}

function table_end()
{
    return "
            </td>
        </tr>
    </table>\n";
}


function form_begin( $border, $error )
{
    return "<form method='post' onsubmit='return checkForm(this);'>
                <table width=80% cellpadding='5px' border='$border' align='center'>
                    <tr>
                         <td colspan='2' align='center'>$error</td>
                    </tr>
                    <tr>
                        <td>
    ";
}

function form_end()
{
    return "
                        </td>
                     </tr>
                    <tr>
                        <td align='center' colspan='2'>
                        <input type='submit' name='exec_debit' value='Выполнить зачисление'>
                        </td>
                    </tr>
                </table>
            </form>\n";
}


function form_money_input($label, $fieldid )
{
    return "
            <table border=0>
                    <tr>
                        <td align='right'>$label</td>
                        <td align='left'><input type='text' name='$fieldid' value='0'></td>
                        <td align='left'>рублей</td>
                    </tr>
            </table>
    ";
}

function print_error( $errMsg, $errDetail )
{
	$error = "<br /><span style='color: red;'>$errMsg</span><br />($errDetail)<br />";
	echo $error;
}

?>
