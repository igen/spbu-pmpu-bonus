<?php
$root = 'http://'.$_SERVER['SERVER_NAME'];
$doc_root = $_SERVER['DOCUMENT_ROOT'];


// MySQL Settings
$dblocation = "localhost";
$dbname = "bonus";
$dbuser = "admin";
$dbpasswd = "pmpu";

// отключение вывода warning
include error_reporting(0);
// включение границ всех таблиц для тестирования
$config_table_border_value = '0';

// all Dbs
$config_table_criteria        = 'criteria';
$config_table_criteria_fields = 'criteria_fields';
$config_table_teachers        = 'teachers';
$config_table_active_teachers = 'active_teachers';

// criteria Dbs
$config_table_stream_lectures = 'cr_stream_lectures';
$config_table_certifying_commission = 'cr_certifying_commission';
$config_table_diplomas_foreign = 'cr_diplomas_foreign';
$config_table_diplomas_russian = 'cr_diplomas_russian';
$config_table_educational_programs = 'cr_educational_programs';
$config_table_electronic_textbook = 'cr_electronic_textbook';
$config_table_facultatives = 'cr_facultatives';
$config_table_lab_works = 'cr_lab_works';
$config_table_special_course = 'cr_special_course';
$config_table_special_sem_not_stream = 'cr_special_sem_not_stream';
$config_table_special_sem_stream = 'cr_special_sem_stream';
$config_table_teachers_30 = 'cr_teachers_30';
$config_table_teachers_35 = 'cr_teachers_35';
$config_table_teachers_loading_excess = 'cr_teachers_loading_excess';
$config_table_textbook_stamp = 'cr_textbook_stamp';
$config_table_tutorials = 'cr_tutorials';


$config_table_news = 'config_table_news';

$config_table_debit = 'debit';

// --

$config_array_new_modified = array("","Новый","Модифицированный");
$config_array_semester_year = array("","Семестровый","Годовой");
$config_array_year_of_approbation = array("","Первый","Второй","Третий");
$config_array_level = array("","Бакалавр","Магистр");
$config_array_language = array("","Русский","Английский");
$config_array_trend = array("","Специализ.","Траектория", "Магистерская прогр.");
$config_array_month = array("","Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь");
$config_array_teacher_degree = array("","к. ф-м. н.","к. т.н.");
$config_array_teacher_rate = array("","0.25","0.5","1","1.5","2");
$config_array_year_of_publication = array("","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030");
$config_array_specialty_code = array("","10300","10400", "10900");



?>
