function checkForm(form) {

    var el, // Сам элемент
        elName, // Имя элемента формы
        value, // Значение
        type; // Атрибут type для input-ов

    var errorList = [];

   for (var i = 0; i < form.elements.length; i++) {
        el = form.elements[i];
        elName = el.nodeName.toLowerCase();
        value = el.value;
        if (elName == "input") { // INPUT

            type = el.type.toLowerCase();


            switch (type) {
                case "text" :
                    if (value == "")
                    {
                        polelabel = document.getElementById(el.name+'_label').innerText;
                        errorList.push('Не заполнено поле '+polelabel);
                    }
                    break;
                default :
                break;
            }
        } else if (elName == "textarea") { // TEXTAREA
            if (value == "") errorList.push(1);
        } else if (elName == "select") { // SELECT
            if (value == 0)
            {
                polelabel = document.getElementById(el.name+'_label').innerText;
                errorList.push('Не выбрано '+polelabel);
                //errorList.push(2);
            }
        } else {
            // Обнаружен неизвестный элемент ;)
        }
    }


    if (!errorList.length) return true;

    var errorMsg = "При заполнении формы допущены следующие ошибки:\n\n";
    for (i = 0; i < errorList.length; i++) {
        errorMsg += errorList[i] + "\n";
    }
    alert(errorMsg);
    return false;
}