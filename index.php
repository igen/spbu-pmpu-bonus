<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
include("$_SERVER[DOCUMENT_ROOT]/modules/config.php");
include("$doc_root/modules/dbConnect.php");
include("$doc_root/modules/tableCreate.php");
include("$doc_root/modules/login/loginCheck.php");
include("$doc_root/modules/libmail.php");
include("$doc_root/modules/functions/globalFunctions.php");
include("$doc_root/modules/head.php");

include("$doc_root/modules/html/mainTableStart.php");
include("$doc_root/modules/html/mainTopTitle.php");
include("$doc_root/pages/main.php");
include("$doc_root/modules/html/mainTableEnd.php");
?>